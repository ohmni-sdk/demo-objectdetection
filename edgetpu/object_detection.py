import argparse
import platform
import subprocess
import signal
from edgetpu.detection.engine import DetectionEngine
from PIL import Image
from PIL import ImageDraw
import json
import socket
import os, os.path
import time
from enum import Enum
from struct import *

# Open connection to bot shell
botshell = socket.socket( socket.AF_UNIX, socket.SOCK_STREAM )
botshell.connect("/app/bot_shell.sock")
#botshell.sendall(b"say hello\n")
botshell.sendall(b"wake_head\n")

if os.path.exists( "/dev/libcamera_stream" ):
  os.remove( "/dev/libcamera_stream" )

print("Opening socket...")
server = socket.socket( socket.AF_UNIX, socket.SOCK_DGRAM )
server.bind("/dev/libcamera_stream")
os.chown("/dev/libcamera_stream", 1047, 1047)
class SockState(Enum):
  SEARCHING = 1
  FILLING = 2

# Function to rad labels from text files
def ReadLabelFile(file_path):
  with open(file_path, 'r') as f:
    lines = f.readlines()

  ret = {}
  for line in lines:
    pair = line.strip().split(maxsplit=1)
    ret[int(pair[0])] = pair[1].strip()
  return ret

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--model', help='Path of the detection model.', required=True)
  parser.add_argument(
      '--label', help='Path of the labels file.')
  args = parser.parse_args()

  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = ReadLabelFile(args.label) if args.label else None

  state = SockState.SEARCHING
  imgdata = None
  framewidth = 0
  frameheight = 0
  frameformat = 0
  framesize = 0

  print("Listening...")
  server.settimeout(0.5)
  while True:

    try:
      datagram = server.recv( 65536 )
    except socket.timeout:
      continue

    if not datagram:
      break
    # Handle based on state machine
    if state == SockState.SEARCHING:

      # Check for non-control packets
      if len(datagram) < 12 or len(datagram) > 64:
        continue

      # Check for magic
      if not datagram.startswith(b'OHMNICAM'):
        continue

      # Unpack the bytes here now for the message type
      msgtype = unpack("I", datagram[8:12]) 
      if msgtype[0] == 1:
        params = unpack("IIII", datagram[12:28])
        #print("Got frame start msg:", params)

        state = SockState.FILLING
        imgdata = bytearray()

        framewidth = params[0]
        frameheight = params[1]
        frameformat = params[2]
        framesize = params[3]

    # Filling image buffer now
    elif state == SockState.FILLING:

      # Append to buffer here
      imgdata.extend(datagram)

      # Check size
      if len(imgdata) < framesize:
        continue

      # Resize and submit
      imgbytes = bytes(imgdata)
      newim = Image.frombytes("L", (framewidth, frameheight), imgbytes, "raw", "L")
      rgbim = newim.convert("RGB")

      # Run inference.
      ans = engine.DetectWithImage(rgbim, threshold=0.05, keep_aspect_ratio=True,
                                   relative_coord=False, top_k=10)

      # Best face to follow
      best = None

      # Display result.
      currfaces = 0
      if ans:
        currfaces = len(ans)
        objects = []
        print ('-----------------------------------------')
        for obj in ans:
          box = obj.bounding_box.flatten().tolist()
          # if (obj.score > 0.4):
          print("bounding_box", obj.bounding_box.flatten().tolist())
          box = obj.bounding_box.flatten().tolist()
          print("score", float(obj.score))
          print("label", labels[obj.label_id])
          objects.append({"score": float(obj.score), "label": labels[obj.label_id], "box": box})
        returnedData = {"type": "object_detection", "msg": objects}
        standAloneCommand = "send_to_stand_alone_api " + json.dumps(returnedData) + "\n"
        inCallCommand = "send_to_in_call_api " + json.dumps(returnedData) + "\n"
        if len(objects) >= 1:
          botshell.sendall(standAloneCommand.encode('UTF-8'))
          botshell.sendall(inCallCommand.encode('UTF-8'))
      else:
        print("No detections.")
      # Go back to initial state
      state = SockState.SEARCHING
      #print("Got complete frame")

  print("-" * 20)
  print("Shutting down...")
  server.close()
  botshell.close();
  os.remove( "/dev/libcamera_stream" )
  print("Done")

if __name__ == '__main__':
  main()