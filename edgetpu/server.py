import argparse
import zmq
import json
import numpy as np

from time import sleep
from PIL import Image
from edgetpu.detection.engine import DetectionEngine

# Function to rad labels from text files
def ReadLabelFile(file_path):
  with open(file_path, 'r') as f:
    lines = f.readlines()

  ret = {}
  for line in lines:
    pair = line.strip().split(maxsplit=1)
    ret[int(pair[0])] = pair[1].strip()
  
  return ret

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--model', help='Path of the detection model.', required=True, type=str)
  parser.add_argument(
      '--label', help='Path of the labels file.', required=True, type=str)
  parser.add_argument(
      '--socket', help='Path of the labels file.', 
      type=str, default="tcp://0.0.0.0:5544")
  args = parser.parse_args()

  # Intiailize the socket
  context = zmq.Context()
  socket = context.socket(zmq.REP)
  socket.bind(args.socket)
  sleep(1)

  # Initialize the detection engine
  engine = DetectionEngine(args.model)
  labels = ReadLabelFile(args.label) if args.label else None

  # Listen the requests and make predictions
  print("Listening ...")
  try:
    while True:
      response = {}
      try:
        image = socket.recv_pyobj()
        print("Receiving a request")

        # Run inference
        ans = engine.DetectWithImage(image, threshold=0.05, keep_aspect_ratio=True,
                                    relative_coord=False, top_k=10)

        # Get the bounding boxes and the corresponding score
        predictions = []
        if ans:
          for obj in ans:
            # print(obj)
            predictions.append({
              "bounding_box": obj.bounding_box.flatten().tolist(),
              "score": float(obj.score),
              "label": labels[obj.label_id]
            })

        response['result'] = "success"
        response['predictions'] = predictions
        print("Done!\n")
      except Exception as err:
        print("Failed! Error {}\n".format(err))
        response['result'] = 'failed'
        response['error'] = err


      # Send back the results to the client
      socket.send_json(response)
  except (KeyboardInterrupt, SystemExit):
    print("Stopping the server")
    pass

if __name__ == "__main__":
  main()
